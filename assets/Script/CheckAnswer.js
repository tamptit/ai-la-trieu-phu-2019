const GameConfig = require('./GameConfig')

cc.Class({
    extends: cc.Component,

    properties: {
        btnlock :{
            default: null,
            type : cc.Button
        },
        cheerAudio: {
            default: [],
            type: cc.AudioClip
        }
    },
    onLoad(){
        this.booleanKey = null
        this.loadQuestion = this.node.getComponent('LoadQuestion')
    },
    gameOver(){
        cc.audioEngine.playEffect(this.cheerAudio[4] , false)
        setTimeout( ()=>{
            cc.director.loadScene('GameOver')
        }, 4000);
        
        
    },

    CheckKey(e, data){
        //this.currentCheck = this.loadQuestion.current
        let currentCheck = this.loadQuestion.current
        cc.log("current Check " + currentCheck)
        cc.log(e, + " data" + data)
        //this.btnlock.active = true
        if (data != GameConfig.dataQuestion[currentCheck].key){        
            this.booleanKey = false;
            
            this.gameOver();

        }
        else {
            
            if (data ==0 ){
                cc.audioEngine.playEffect(this.cheerAudio[0], false);
            } 
            if (data == 1){
                cc.audioEngine.playEffect(this.cheerAudio[1], false);
            }
            if (data == 2){
                cc.audioEngine.playEffect(this.cheerAudio[2], false);
            }
            if (data == 3) {
                cc.audioEngine.playEffect(this.cheerAudio[3], false);
            }
            this.booleanKey = true;
            setTimeout( ()=>{
                this.loadQuestion.Level()  
            }, 5000 )
            
        }
     },
    
    
});
