var gameConfig = require('./GameConfig')
cc.Class({
    extends: cc.Component,

    properties: {
        AdviceNode:{
            default: null,
            type: cc.Node
        },
        lbAdvice:{
            default: null,
            type : cc.Label
        },
        HelpCallNode:{
            default: null,
            type: cc.Node
        }
    },

    onLoad() {
        this.getIDQuestion = this.node.parent.getComponent('LoadQuestion')
        this.used = true
    },
    HelpCallfunction(e, data) {
        let currentQuestion = this.getIDQuestion.current
        let helpKey = gameConfig.dataQuestion[currentQuestion].key
        let key 
        if (helpKey == 0){
            key = "A."
        }else if (helpKey == 1){
            key = "B."
        }else if (helpKey == 2){
            key = "C."
        }else {
            key = "D."
        }

        if (this.node.children[0].active == true && this.used == true) {
            
            if (data == 9 || data == 7 || data == 6 || data == 5 || data == 4) {
                this.node.children[1].active = true
                this.lbAdvice.string = e.currentTarget.name +  " khuyên bạn chọn " + key + gameConfig.dataQuestion[currentQuestion].answer[helpKey]
                cc.log(e, data)
                this.used = false;
            }
            if (data == 8 ) {
                this.node.children[1].active = true
                if (helpKey == 1) {
                    this.lbAdvice.string = e.currentTarget.name  + " khuyên bạn chọn " + key + gameConfig.dataQuestion[currentQuestion].answer[2]
                    
                } else if (helpKey == 2) {
                    this.lbAdvice.string = e.currentTarget.name  + " khuyên bạn chọn " + key + gameConfig.dataQuestion[currentQuestion].answer[3]
                    
                } else if (helpKey == 0) {
                    this.lbAdvice.string = e.currentTarget.name  + " khuyên bạn chọn " + key +  gameConfig.dataQuestion[currentQuestion].answer[1]
                    
                } else {
                    this.lbAdvice.string = e.currentTarget.name  + " khuyên bạn chọn " + key + gameConfig.dataQuestion[currentQuestion].answer[0]
                    
                }
                this.used = false;
            }
        }

    },

    closePanelBlue(){
        this.node.children[1].active = false
        this.node.children[0].active = false
    },
    togglelHelpCallNode() {
        if (this.used){
            this.node.children[0].active = !this.node.children[0].active    
        }
    },

    start () {

    },

   
});
