
const GameConfig = require('./GameConfig')

cc.Class({
    extends: cc.Component,

    properties: {
        lbAnswerA: {
            default: null,
            type: cc.Label
        },
        lbAnswerB: {
            default: null,
            type: cc.Label
        },
        lbAnswerC: {
            default: null,
            type: cc.Label
        },
        lbAnswerD: {
            default: null,
            type: cc.Label
        },
        lbQuestion: {
            default: null,
            type: cc.Label
        },
        lbTime: {
            default : null,
            type : cc.Label
        },
        lbLevel:{
            default : null,
            type : cc.Label
        },
        lbScore:{
            default : null,
            type : cc.Label
        },
        
       
    },
    
    // checkRandom(){
    //     var arrCheckrandom = [];
    //     var numofQuestion = GameConfig.dataQuestion.length
    //     if (!arrCheckrandom.length){
    //         for (var i =0; i< numofQuestion; i++){
    //             arrCheckrandom.push(i)
    //         }
    //     }

    //     var i = Math.floor(Math.random()*arrCheckrandom.length );
    //     var val = arrCheckrandom[i];
    //     arrCheckrandom.splice(i,1) // remove i have been used
    //     return val;
    // },
    
    
  
    onLoad() {
        this.level = 1;
        this.timer = 31;
        // this.booleanKey = false;
        this.earMoney = 0;
        this.arr = [];
        this.LoadQuestion();
        this.checkAnswer = this.node.getComponent('CheckAnswer')
    
        window.G = {
            money: null,
            numCross: null,
        }
    },


    CheckRandomfunction(){
        let numofQuestion = GameConfig.dataQuestion.length   // do dai Gamecofig
        let i  = Math.floor(Math.random()*numofQuestion )    
 
        while(this.arr.indexOf(i) >= 0) {                   // kiem tra i co trong mang chua
            i = Math.floor(Math.random() * numofQuestion)
        }
        this.arr.push(i)                                   // chua co thi tra ve + push vao arr
        console.log("arr = " + this.arr)
        return i;
    },

    LoadQuestion( ){
        if (this.arr.length == 16){
            this.gameOver()
        }else{
            let numofQuestion = GameConfig.dataQuestion.length 
            let i = this.CheckRandomfunction()                        // lay random so i chua dc random
            this.current = i;
            this.lbQuestion.string = GameConfig.dataQuestion[i].question  
            this.lbAnswerA.string = "A. " + GameConfig.dataQuestion[i].answer[0]
            this.lbAnswerB.string = "B. " + GameConfig.dataQuestion[i].answer[1]
            this.lbAnswerC.string = "C. " + GameConfig.dataQuestion[i].answer[2]
            this.lbAnswerD.string = "D. " + GameConfig.dataQuestion[i].answer[3]
        }
        
    },
/*
    var uniqueRandoms = [];
    var numRandoms = 5;
    function makeUniqueRandom() {
    // refill the array if needed
    if (!uniqueRandoms.length) {
        for (var i = 0; i < numRandoms; i++) {
            uniqueRandoms.push(i);
        }
    }
    var index = Math.floor(Math.random() * uniqueRandoms.length);
    var val = uniqueRandoms[index];
    // now remove that value from the array
    uniqueRandoms.splice(index, 1);
    return val;
    }
*/
    
    
//---------Checkey, Time, Level, Earn---------------------------------------//
    // CheckKey(e, data){
    //    //console.log(e,data)
        
    //    this.e = e ; this.data = data
    //   // console.log(e,data)
    //     if (data != GameConfig.dataQuestion[this.current].key){        
    //         alert("Sai Rồiii")
    //         this.booleanKey = false;
    //         //this.gameOver();
    //     }
    //     else {
    //        // alert("Chính xác")
    //         //this.timer =11;
    //         this.booleanKey = true
    //         // setTimeout(
    //         //     this.UpdateTime(),
    //         //     function(){ alert("Hello"); }, 
    //         //     this.Level()
    //         // ,3000)
            
    //         //this.UpdateTime();
    //     }
    // },

    UpdateTime(){
        this.timer = 30.5;
    },

    
    gameOver(){
        cc.director.loadScene('GameOver')
    },
    EarnMoney( lv){
        if (lv == 1){
            this.earMoney = 200
        }else if(lv == 2){
            this.earMoney = 400
        }else if (lv == 3){
            this.earMoney = 600
        }else if (lv == 4){
            this.earMoney = 1000
        }else if(lv == 5){
            this.earMoney = 2000
        }else if (lv == 6){
            this.earMoney = 3000
        }else if (lv == 7){
            this.earMoney = 6000
        }else if (lv == 8){
            this.earMoney = 10000
        }else if (lv == 9){
            this.earMoney = 14000 
        }else if (lv == 10){
            this.earMoney = 22000
        }else if (lv == 11){
            this.earMoney = 30000
        }else if (lv == 12){
            this.earMoney = 40000
        }else if (lv == 13){
            this.earMoney = 60000
        }else if (lv == 14){
            this.earMoney = 85000
        }else if (lv == 15){
            this.earMoney = 150000
        }
    },

    Level(){
        if (this.checkAnswer.booleanKey){
            this.EarnMoney(this.level)
            this.level += 1; 
            this.LoadQuestion();
            this.UpdateTime();
            
        }
        var lv = this.level;
        this.lbLevel.string = "Câu " + lv   // `Cau so: ${this.level}`
        this.lbScore.string = `Tiền: ${this.earMoney}$`
        G.numCross = this.level - 1
        G.money = this.earMoney 
        cc.log(G.moneyEar + " " + G.numQuetionCross)
        
    },

//---------------------- Funtion Support---------------//

    // click 1 lan active click them thi active

    


//-------------------------------------------------------//
    update(dt){
        if (this.timer > 0) {
            this.lbTime.string = Math.floor(this.timer).toString();
            this.timer -= dt;
           // cc.log(this.timer)
            if (this.timer < 0) {
               // this.gameOver()
            }
        }
        if (this.checkAnswer.booleanKey == false){

        }
    }

});
