module.exports.dataQuestion = [
    {
        id: 0,
        question: "Victor Hugo là ai?",
        answer: ["Người mẫu ảnh" , "Nhà thiết kế", "Vận động viên ", "Nhà văn"],
        key: 3
    },

    {
        id: 1,
        question : "London là thủ đô nước nào?",
        answer : ["Mỹ", "Nga", "Trung Quốc", "Anh"],
        key: 3
    },

    {
        id: 2,
        question : "Đâu là đặc sản của Huế",
        answer : ["Cơm nguội", "Cơm hến", "Cơm giò", "Cơm khê"],
        key: 1
    },
    {
        id: 3,
        question : "Đâu là bài thơ nổi tiếng Nguyễn Bính?",
        answer : ["Tương thân", "Tương đương", "Tương Tư", "Tương đồng"],
        key: 2
    },
    {
        id: 4,
        question : " Đại Ngu là quốc hiệu của triều đại nào?",
        answer : ["Triều Ngô", "Triều Hồ", "Triều Lê", "Triều Lý"],
        key: 1
    },
    {
        id: 5,
        question : "Lan has just told to her friends a secret,\nand now she regrets ...",
        answer : ["to tell", "told", "telling", "has told"],
        key: 2
    },
    {
        id: 6,
        question : "Đâu không phải là tên một giải bóng đá",
        answer : ["World Cup", "Cúp C1", "Euro", "Thomas Cup"],
        key: 3
    },
    {
        id: 7,
        question : "Nấu canh cua với gì?",
        answer : ["Củ cải", "Mộc nhĩ", "Rau đay", "Súp lơ"],
        key: 2
    },
    {
        id: 8,
        question : "Ba thương con vì con giống mẹ.\nMẹ thương con vì con giống...",
        answer : ["Ông hàng xóm", "Ba", "Chú cạnh nhà", "Bác đầu ngõ"],
        key: 1
    },
    {
        id: 9,
        question : "\"Điêu tàn\" là tập thơ đầu tiền của nhà thơ nào?",
        answer : ["Chế Lan Viên", "Bích Khê", "Thế Lữ", "Anh Thơ"],
        key: 2
    },
    {
        id: 10,
        question : "Niên hiệu của Đinh Tiên Hoàng gọi là gì",
        answer : ["Thuận Thiên", "Thiên Phúc", "Thái Bình", "Đại Đức"],
        key: 2
    },
    {
        id: 11,
        question : "Sông Bến Hải nằm ở tỉnh nào",
        answer : ["Quảng Bình", "Quảng Ngãi", "Quãng Trị", "Quảng Ninh"],
        key: 2
    },
    {
        id: 12,
        question : "Cổng duy nhất trong 5 cổng Hoàng thành Thăng Long \ncòn sót lại đến ngày nay",
        answer : ["Cửa Đông ", "Cửa Nam", "Cửa Tây", "Cửa Bắc"],
        key: 1
    },
    {
        id: 13,
        question : "Edo từng là tên gọi cũ của thành phố nào Nhật Bản",
        answer : ["Kobe", "Kyoto", "Tokyo", "Osaka"],
        key: 2
    },
    {
        id: 14,
        question : "Hiện tại Quận huyện nào ở Hà Nội có diện tích lớn nhất ",
        answer : ["Quận Long Biên", "Huyện Ba Vì", "Huyện Sóc Sơn", "Huyện Chương Mỹ"],
        key: 2
    },
    {
        id: 15,
        question : "1 + 1 bằng mấy ?",
        answer : ["2 Easy", "3 IQ200", "Dễ khỏi trả lời", "Tự biết không hỏi"],
        key: 1
    },



];

