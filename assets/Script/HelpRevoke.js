var gameConfig = require('./GameConfig')

/*
var label = cc.Class({
    properties: {
        label: {
            default : null,
            type : cc.Label
        },
        name: {
            default: '',
            type: String
        }
    }
})
*/

cc.Class({
    extends: cc.Component,

    properties: {
        HelpRevokeNode: {
            default: null,
            type: cc.Node
        },
        btnAnswer:{
            default : [],
            type : cc.Label
        }
    },

    onLoad(){
        this.getIDQuestion = this.node.parent.getComponent('LoadQuestion')
        this.time = 1;
    },

    CheckTouchRevoke(){
        if (this.time){
            this.HelpRevokeee()
        }
        this.time = 0;
    },
    
    /*
    HelpRevokefuntion() {
        var currentQuestion = this.getIDQuestion.current
        var helpKey = gameConfig.dataQuestion[currentQuestion].key
        var i = Math.floor(Math.random() * 10);
        if (helpKey == 2) {
            this.getIDQuestion.lbAnswerC.string = "C. " + gameConfig.dataQuestion[currentQuestion].answer[2]
            if (i > 6) {
                this.getIDQuestion.lbAnswerA.string = "A. " + gameConfig.dataQuestion[currentQuestion].answer[0]
                this.getIDQuestion.lbAnswerB.string = "B. "
                this.getIDQuestion.lbAnswerD.string = "D. "
            }
            if (i <= 6 && i > 3) {
                this.getIDQuestion.lbAnswerA.string = "A. "
                this.getIDQuestion.lbAnswerB.string = "B. "
                this.getIDQuestion.lbAnswerD.string = "D. " + gameConfig.dataQuestion[currentQuestion].answer[3]
            }
            if (i <= 3) {
                this.getIDQuestion.lbAnswerA.string = "A. "
                this.getIDQuestion.lbAnswerB.string = "B. " + gameConfig.dataQuestion[currentQuestion].answer[1]
                this.getIDQuestion.lbAnswerD.string = "D. "
            }
        }
        if (helpKey == 3) {
            this.getIDQuestion.lbAnswerD.string = "D. " + gameConfig.dataQuestion[currentQuestion].answer[3]
            if (i > 6) {
                this.getIDQuestion.lbAnswerA.string = "A. " + gameConfig.dataQuestion[currentQuestion].answer[0]
                this.getIDQuestion.lbAnswerB.string = "B. "
                this.getIDQuestion.lbAnswerC.string = "C. "
            }
            if (i <= 6 && i > 3) {
                this.getIDQuestion.lbAnswerA.string = "A. "
                this.getIDQuestion.lbAnswerB.string = "B. "
                this.getIDQuestion.lbAnswerC.string = "C. " + gameConfig.dataQuestion[currentQuestion].answer[2]
            }
            if (i <= 3) {
                this.getIDQuestion.lbAnswerA.string = "A. "
                this.getIDQuestion.lbAnswerB.string = "B. " + gameConfig.dataQuestion[currentQuestion].answer[1]
                this.getIDQuestion.lbAnswerC.string = "C. "
            }
        }
        if (helpKey == 1) {
            this.getIDQuestion.lbAnswerB.string = "B. " + gameConfig.dataQuestion[currentQuestion].answer[1]
            if (i > 6) {
                this.getIDQuestion.lbAnswerA.string = "A. " + gameConfig.dataQuestion[currentQuestion].answer[0]
                this.getIDQuestion.lbAnswerC.string = "C. "
                this.getIDQuestion.lbAnswerD.string = "D. "
            }
            if (i <= 6 && i > 3) {
                this.getIDQuestion.lbAnswerA.string = "A. "
                this.getIDQuestion.lbAnswerC.string = "C. "
                this.getIDQuestion.lbAnswerD.string = "D. " + gameConfig.dataQuestion[currentQuestion].answer[3]
            }
            if (i <= 3) {
                this.getIDQuestion.lbAnswerA.string = "A. "
                this.getIDQuestion.lbAnswerC.string = "C. " + gameConfig.dataQuestion[currentQuestion].answer[2]
                this.getIDQuestion.lbAnswerD.string = "D. "
            }
        }
        if (helpKey == 0) {
            this.getIDQuestion.lbAnswerA.string = "A. " + gameConfig.dataQuestion[currentQuestion].answer[0]
            if (i > 6) {
                this.getIDQuestion.lbAnswerB.string = "B. " + gameConfig.dataQuestion[currentQuestion].answer[1]
                this.getIDQuestion.lbAnswerC.string = "C. "
                this.getIDQuestion.lbAnswerD.string = "D. "
            }
            if (i <= 6 && i > 3) {
                this.getIDQuestion.lbAnswerB.string = "B. "
                this.getIDQuestion.lbAnswerC.string = "C. "
                this.getIDQuestion.lbAnswerD.string = "D. " + gameConfig.dataQuestion[currentQuestion].answer[3]
            }
            if (i <= 3) {
                this.getIDQuestion.lbAnswerB.string = "B. "
                this.getIDQuestion.lbAnswerC.string = "C. " + gameConfig.dataQuestion[currentQuestion].answer[2]
                this.getIDQuestion.lbAnswerD.string = "D. "
            }
        }
        
    },
    */


    HelpRevokeee(){
        
        let arr = [0,1,2,3];
        let currentQuestion = this.getIDQuestion.current;
        let helpKey = gameConfig.dataQuestion[currentQuestion].key
        arr.splice(helpKey, 1)
        let moreAnswer = arr[Math.random() * 3 | 0]
        cc.log("moreAnwser " + moreAnswer + " key " + helpKey)
        //var arr = []; arr = this.btnAnswerNode
        arr.forEach((item, index) => {
            if (item != helpKey && item != moreAnswer){
                //this.btnAnswer.string = ''
                cc.log("item " + item)
                this.btnAnswer[item].string =''
            }
        });
        
        
    },

    
   
});
