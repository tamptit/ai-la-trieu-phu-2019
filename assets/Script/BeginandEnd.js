
cc.Class({
    extends: cc.Component,

    properties: {
        btnStart :{
            default : null,
            type : cc.Button
        },
         lbGetout :{
           default : null,
           type : cc.Label
        },
        gofindMillionaireAudio: {
            default: null,
            type: cc.AudioClip
        },
        
        loseAudio: {
            default: null,
            type: cc.AudioClip
        }
    },

    BeginNow(){
        cc.audioEngine.playEffect(this.gofindMillionaireAudio, false);
        setTimeout( () => {
            cc.director.loadScene('Gameplay')
        }, 2000);
    },

    GetGameOver(){
        cc.log(G.numCross +" - " +G.money)
        if (G.numCross >=1 ){
            this.lbGetout.string = "BẠN VƯỢT QUA CÂU SỐ : " + G.numCross 
            + "\n NHẬN ĐƯỢC " + G.money + " $$$."
        }
    },
    
    onLoad(){
        this.GetGameOver()
    }

});
