const gameConfig = require('./GameConfig')

cc.Class({
    extends: cc.Component,

    properties: {
        speed: 10,
        barA: {
            type: cc.ProgressBar,
            default: null
        },
        barB: {
            type: cc.ProgressBar,
            default: null
        },
        barC: {
            type: cc.ProgressBar,
            default: null
        },
        barD: {
            type: cc.ProgressBar,
            default: null
        },
        lbPrecentA:{
            type : cc.Label,
            default: null
        },
        lbPrecentB:{
            type : cc.Label,
            default: null
        },
        lbPrecentC:{
            type : cc.Label,
            default: null
        },
        lbPrecentD:{
            type : cc.Label,
            default: null
        }
    },

    onLoad () {
        this._pingpong = true;
        this.progress = 0;
        this.barA.progress = 0;
        this.barB.progress = 0;
        this.barC.progress = 0;
        this.barD.progress = 0;
        this.loadQuestion = this.node.parent.getComponent('LoadQuestion')
        
    },

    toggleHelpAudience() {
        this.node.children[0].active = !this.node.children[0].active
        if (this.node.children[0].active == true){
            this.PercentProgress();
        }
    },

    update: function (dt) {
        if (this.node.children[0].active == true){
            this._updateProgressBarAnswerA(this.barA, dt);
            this._updateProgressBarAnswerB(this.barB, dt);
            this._updateProgressBarAnswerC(this.barC, dt);
            this._updateProgressBarAnswerD(this.barD, dt);
        }
    },

    PercentProgress(){
        let currentQuestion = this.loadQuestion.current
        if (gameConfig.dataQuestion[currentQuestion].key == 0){
            this.A = Math.floor(Math.random() * (60-40) + 40 ) 
            this.B  = Math.floor(Math.random() * (20-10) )
            this.D = Math.floor(Math.random() * (20-10) ) 
            this.C = 100 - this.A- this.B - this.D
        
        }else if (gameConfig.dataQuestion[currentQuestion].key == 1 ){
            this.B = Math.floor(Math.random() * (60-40) + 40 ) 
            this.A = Math.floor(Math.random() * (20-10) )
            this.C = Math.floor(Math.random() * (20-10) ) 
            this.D = 100- this.B - this.A - this.C
        }else if (gameConfig.dataQuestion[currentQuestion].key == 2 ){
            this.C = Math.floor(Math.random() * (60-40) + 40 ) 
            this.D = Math.floor(Math.random() * (20-10) ) 
            this.B = Math.floor(Math.random() * (20-10) ) 
            this.A = 100 - this.C - this.D - this.B
        }else {
            this.D = Math.floor(Math.random() * (60-40) + 40 ) 
            this.B = Math.floor(Math.random() * (20-10) ) 
            this.C = Math.floor(Math.random() * (20-10) ) 
            this.A = 100 - this.D - this.C - this.B
        }
        cc.log(this.A, this.B, this.C, this.D)
        this.lbPrecentA.string = `A: ${this.A}%`
        this.lbPrecentB.string = `B: ${this.B}%`
        this.lbPrecentC.string = `C: ${this.C}%`
        this.lbPrecentD.string = `D: ${this.D}%`
    },


    _updateProgressBarAnswerA(progressBarAnswer, dt){
        let a = this.A/100
        let progress = progressBarAnswer.progress;
        if(progress < a && this._pingpong){
            progress += dt * this.speed;
        }
        else {
            
        }
        progressBarAnswer.progress = progress;
        
    },
    
    _updateProgressBarAnswerB(progressBarAnswer, dt){
        let b = this.B/100
        let progress = progressBarAnswer.progress;
        if(progress < b && this._pingpong){
            progress += dt * this.speed;
        }
        else {
        
        }
        progressBarAnswer.progress = progress;
    },

    _updateProgressBarAnswerC(progressBarAnswer, dt){
        let c = this.C/100
        let progress = progressBarAnswer.progress;
        if(progress < c && this._pingpong){
            progress += dt * this.speed;
        }
        else {
         
        }
        progressBarAnswer.progress = progress;
    },

    _updateProgressBarAnswerD(progressBarAnswer, dt){
        let d = this.D/100
        let progress = progressBarAnswer.progress;
        if(progress < d && this._pingpong){
            progress += dt * this.speed;
        }
        else {
            
        }
        progressBarAnswer.progress = progress;
    },



});
